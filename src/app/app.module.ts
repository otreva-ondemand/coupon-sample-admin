import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { AngularFireModule, AuthProviders, AuthMethods } from 'angularfire2';
import { MessagesModule } from 'primeng/primeng';

import { AdminModule } from './admin/admin.module';
import { AppRoutingModule } from './app-routing.module';
import { DatabaseService } from './database.service';
import { AuthGuard } from './auth-guard.service';
import { AuthService } from './auth.service';
import { StorageService } from './storage.service';

import { AppComponent } from './app.component';
import { LoginComponent } from './login/login.component';

export const firebaseConfig = {
  apiKey: 'AIzaSyAlAqSZiViCpsyLb_4ghJ_68KTd1v1GlRw',
  authDomain: 'coupon-sample.firebaseapp.com',
  databaseURL: 'https://coupon-sample.firebaseio.com',
  storageBucket: 'coupon-sample.appspot.com'
}

const firebaseAuthConfig = {
  provider: AuthProviders.Password,
  method: AuthMethods.Password
}

@NgModule({
  declarations: [AppComponent, LoginComponent],
  imports: [
    BrowserModule,
    FormsModule,
    HttpModule,
    AppRoutingModule,
    AngularFireModule.initializeApp(firebaseConfig, firebaseAuthConfig),
    AdminModule,
    MessagesModule
  ],
  providers: [DatabaseService, AuthGuard, AuthService, StorageService],
  bootstrap: [AppComponent]
})
export class AppModule {}
