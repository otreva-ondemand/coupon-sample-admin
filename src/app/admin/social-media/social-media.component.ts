import { Component } from '@angular/core';

import { DatabaseService } from '../../database.service';

@Component({
  selector: 'app-social-media',
  templateUrl: './social-media.component.html',
  styleUrls: ['./social-media.component.scss']
})
export class SocialMediaComponent {
  public messages = [];
  public displayLoading = false;
  public urls: any = {
    facebook: '',
    twitter: '',
    googlePlus: '',
    youTube: '',
    instagram: '',
    pinterest: '',
    tumblr: '',
    website: ''
  };

  constructor(private databaseService: DatabaseService) {
    this.databaseService.getSocial().then(urls => {
      this.urls = urls;
    });
  }

  saveButtonClicked() {
    this.messages = [];
    this.displayLoading = true;
    this.databaseService.pushSocial(this.urls).then(() => {
      this.displayLoading = false;
      this.messages.push({
        severity: 'success',
        summary: 'Success!',
        detail: 'Your social media links have been updated'
      });
    }).catch(err => {
      this.displayLoading = false;
      this.messages.push({
        severity: 'error',
        summary: 'Something went wrong!',
        detail: err
      });
    })
  }
}
