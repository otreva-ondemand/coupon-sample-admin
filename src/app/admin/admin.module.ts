import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { ConfirmDialogModule } from 'primeng/primeng';
import { GrowlModule } from 'primeng/primeng';
import { CalendarModule } from 'primeng/primeng';
import { MessagesModule } from 'primeng/primeng';
import { DialogModule } from 'primeng/primeng';

import { AdminRoutingModule } from './admin-routing.module';

import { AdminComponent } from './admin.component';
import { NewsAllComponent } from './news-all/news-all.component';
import { CouponAllComponent } from './coupon-all/coupon-all.component';
import { CouponCreateComponent } from './coupon-create/coupon-create.component';
import { NewsCreateComponent } from './news-create/news-create.component';
import { NewsSingleComponent } from './news-single/news-single.component';
import { CouponSingleComponent } from './coupon-single/coupon-single.component';
import { NewsEditComponent } from './news-edit/news-edit.component';
import { CouponEditComponent } from './coupon-edit/coupon-edit.component';
import { UserAccountComponent } from './user-account/user-account.component';
import { ImageUploadComponent } from './image-upload/image-upload.component';
import { ModalLoadingComponent } from './modal-loading/modal-loading.component';
import { ReversePipe } from './reverse.pipe';
import { SocialMediaComponent } from './social-media/social-media.component';

@NgModule({
  imports: [
    CommonModule,
    AdminRoutingModule,
    FormsModule,
    ConfirmDialogModule,
    GrowlModule,
    CalendarModule,
    MessagesModule,
    DialogModule
  ],
  declarations: [
    NewsAllComponent,
    CouponAllComponent,
    CouponCreateComponent,
    NewsCreateComponent,
    NewsSingleComponent,
    CouponSingleComponent,
    NewsEditComponent,
    AdminComponent,
    CouponEditComponent,
    UserAccountComponent,
    ImageUploadComponent,
    ModalLoadingComponent,
    ReversePipe,
    SocialMediaComponent
  ]
})
export class AdminModule {}
