import { Component } from '@angular/core';
import { Router } from '@angular/router';

import { DatabaseService } from '../../database.service';

@Component({
  templateUrl: './news-create.component.html',
  styleUrls: ['./news-create.component.scss']
})
export class NewsCreateComponent {
  public displayLoading = false;
  public errorMessages = [];
  public newsItem: any = {
    title: '',
    content: '',
    image: '',
    imagePath: ''
  }

  constructor(
    private databaseService: DatabaseService,
    private router: Router
  ) {}

  backToListClicked() {
    this.router.navigate(['./admin/news-all']);
  }

  publishButtonClicked() {
    this.errorMessages = [];
    this.displayLoading = true;

    this.databaseService.pushNewsItem(this.newsItem)
      .then(() => {
        this.displayLoading = false;
        this.router.navigate(['./admin/news-all']);
      })
      .catch(error => {
        this.displayLoading = false;
        this.errorMessages.push({
          severity: 'error',
          summary: 'Something went wrong!',
          detail: error
        })
      })
  }
}
