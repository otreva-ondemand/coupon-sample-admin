import { Component } from '@angular/core';

import { AuthService } from '../../auth.service';

@Component({
  templateUrl: './user-account.component.html',
  styleUrls: ['./user-account.component.scss']
})
export class UserAccountComponent {
  public displayLoading = false;
  public messages = [];
  public showChangePassword = false;
  public userEmail = this.authService.getUserEmail();
  public model = {
    currentPassword: '',
    newPassword: '',
    confirmNewPassword: ''
  }

  constructor(private authService: AuthService) {}

  changePasswordClicked() {
    this.showChangePassword = true;
    this.messages = [];
  }

  cancelChangePasswordClicked() {
    this.showChangePassword = false;
    this.model.currentPassword = '';
    this.model.newPassword = '';
    this.model.confirmNewPassword = '';
    this.messages = [];
  }

  SubmitNewPasswordButtonClicked() {
    this.messages = [];
    this.displayLoading = true;

    this.authService.changePassword(this.model)
      .then(() => {
        this.displayLoading = false;
        this.showChangePassword = false;
        this.messages.push({
          severity: 'success',
          summary: 'WooHoo!',
          detail: 'Password Change successful!'
        })
      })
      .catch(error => {
        this.displayLoading = false;
        this.messages.push({
          severity: 'error',
          summary: 'Something went wrong!',
          detail: error
        })
      })
  }
}
