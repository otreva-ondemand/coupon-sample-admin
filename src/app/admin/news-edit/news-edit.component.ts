import { Component } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

import { DatabaseService } from '../../database.service';

@Component({
  templateUrl: './news-edit.component.html',
  styleUrls: ['./news-edit.component.scss']
})
export class NewsEditComponent {
  public deleteImage = false;
  public displayLoading = false;
  public errorMessages = [];
  public newsId = this.activatedRoute.snapshot.params['id'];
  public newsItem: any = {};

  constructor(
    private activatedRoute: ActivatedRoute,
    private databaseService: DatabaseService,
    private router: Router
  ) {
    this.databaseService.getNewsItem(this.newsId)
      .then(newsItem => {this.newsItem = newsItem})
  }

  cancelButtonClicked() {
    this.router.navigate(['/admin/news/' + this.newsId]);
  }

  deleteImageButtonClicked() {
    this.newsItem.image = '';
    this.deleteImage = true;
  }

  saveButtonClicked() {
    this.displayLoading = true;
    this.errorMessages = [];

    this.databaseService.editNewsItem(this.newsId, this.newsItem, this.deleteImage)
      .then(() => {
        this.displayLoading = false;
        this.router.navigate(['/admin/news/' + this.newsId]);
      })
      .catch(error => {
        this.displayLoading = false;
        this.errorMessages.push({
          severity: 'error',
          summary: 'Something went wrong!',
          detail: error
        })
      })
  }
}
