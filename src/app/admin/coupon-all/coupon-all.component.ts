import { Component } from '@angular/core';
import { DatabaseService } from '../../database.service';

@Component({
  templateUrl: './coupon-all.component.html',
  styleUrls: ['./coupon-all.component.scss']
})
export class CouponAllComponent {
  public coupons;

  constructor(private databaseService: DatabaseService) {
    this.coupons = this.databaseService.getCoupons();
  }
}
