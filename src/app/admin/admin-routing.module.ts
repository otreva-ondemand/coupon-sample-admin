import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { AuthGuard } from '../auth-guard.service';

import { AdminComponent } from './admin.component';
import { NewsAllComponent } from './news-all/news-all.component';
import { CouponAllComponent } from './coupon-all/coupon-all.component';
import { CouponCreateComponent } from './coupon-create/coupon-create.component';
import { NewsCreateComponent } from './news-create/news-create.component';
import { NewsSingleComponent } from './news-single/news-single.component';
import { CouponSingleComponent } from './coupon-single/coupon-single.component';
import { NewsEditComponent } from './news-edit/news-edit.component';
import { CouponEditComponent } from './coupon-edit/coupon-edit.component';
import { UserAccountComponent } from './user-account/user-account.component';
import { SocialMediaComponent } from './social-media/social-media.component';

const adminRoutes: Routes = [
  {
    path: 'admin',
    canActivate: [AuthGuard],
    component: AdminComponent,
    children: [
      {
        path: '',
        redirectTo: 'news-all',
        pathMatch: 'full'
      },
      {
        path: 'coupon-all',
        component: CouponAllComponent
      },
      {
        path: 'coupon-edit/:id',
        component: CouponEditComponent
      },
      {
        path: 'coupon-create',
        component: CouponCreateComponent
      },
      {
        path: 'news-all',
        component: NewsAllComponent
      },
      {
        path: 'news-create',
        component: NewsCreateComponent
      },
      {
        path: 'news/:id',
        component: NewsSingleComponent
      },
      {
        path: 'news-edit/:id',
        component: NewsEditComponent
      },
      {
        path: 'coupon/:id',
        component: CouponSingleComponent
      },
      {
        path: 'user-account',
        component: UserAccountComponent
      },
      {
        path: 'social-media',
        component: SocialMediaComponent
      }
    ]
  }
];

@NgModule({
  imports: [
    RouterModule.forChild(adminRoutes)
  ],
  exports: [
    RouterModule
  ]
})
export class AdminRoutingModule {}
