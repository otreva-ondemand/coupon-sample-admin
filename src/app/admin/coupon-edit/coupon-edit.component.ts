import { Component } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

import { DatabaseService } from '../../database.service';

@Component({
  templateUrl: './coupon-edit.component.html',
  styleUrls: ['./coupon-edit.component.scss']
})
export class CouponEditComponent {
  public deleteImage = false;
  public displayLoading = false;
  public errorMessages = [];
  public couponId = this.activatedRoute.snapshot.params['id'];
  public coupon: any = {};
  public originalCouponCode = '';

  constructor(
    private activatedRoute: ActivatedRoute,
    private databaseService: DatabaseService,
    private router: Router
  ) {
    this.databaseService.getCouponItem(this.couponId).then(coupon => {
      this.coupon = coupon;
      this.originalCouponCode = coupon['code'];
    });
  }

  cancelButtonClicked() {
    this.router.navigate(['/admin/coupon/' + this.couponId]);
  }

  deleteImageButtonClicked() {
    this.coupon.image = '';
    this.deleteImage = true;
  }

  clearStartDate() {
    this.coupon.startDate = '';
  }

  clearExpDate() {
    this.coupon.expDate = '';
  }

  saveButtonClicked() {
    this.displayLoading = true;
    this.errorMessages = [];

    this.databaseService.editCouponItem(this.couponId, this.coupon, this.deleteImage, this.originalCouponCode).then(() => {
      this.displayLoading = false;
      this.router.navigate(['/admin/coupon/' + this.couponId]);
    }).catch(err => {
      this.displayLoading = false;
      this.errorMessages.push({
        severity: 'error',
        summary: 'Something went wrong!',
        detail: err
      });
    });
  }
}
