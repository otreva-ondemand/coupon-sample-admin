import { Component } from '@angular/core';

import { AuthService } from '../auth.service';

@Component({
  templateUrl: './admin.component.html',
  styleUrls: ['./admin.component.scss']
})
export class AdminComponent {
  public isNavOpen = false;
  public userEmail = this.authService.getUserEmail();

  constructor(private authService: AuthService) {}

  navToggleClick() {
    this.isNavOpen = !this.isNavOpen;
  }

  closeSideMenu() {
    this.isNavOpen = false;
  }

  logoutClicked() {
    this.authService.logout();
  }
}
