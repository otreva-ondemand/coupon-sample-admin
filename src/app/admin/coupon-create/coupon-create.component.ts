import { Component } from '@angular/core';
import { Router } from '@angular/router';

import { DatabaseService } from '../../database.service';

@Component({
  templateUrl: './coupon-create.component.html',
  styleUrls: ['./coupon-create.component.scss']
})
export class CouponCreateComponent {
  public displayLoading = false;
  public errorMessages = [];
  public coupon: any = {
    title: '',
    description: '',
    startDate: '',
    expDate: '',
    code: '',
    image: '',
    imagePath: ''
  };

  constructor(
    private databaseService: DatabaseService,
    private router: Router
  ) {}

  backToListClicked() {
    this.router.navigate(['./admin/coupon-all']);
  }

  clearStartDate() {
    this.coupon.startDate = '';
  }

  clearExpDate() {
    this.coupon.expDate = '';
  }

  createButtonClicked() {
    this.errorMessages = [];
    this.displayLoading = true;

    this.databaseService.pushCoupon(this.coupon)
      .then(() => {
        this.displayLoading = false;
        this.router.navigate(['./admin/coupon-all']);
      })
      .catch(error => {
        this.displayLoading = false;
        this.errorMessages.push({
          severity: 'error',
          summary: 'Something went wrong!',
          detail: error
        });
      });
  }
}
