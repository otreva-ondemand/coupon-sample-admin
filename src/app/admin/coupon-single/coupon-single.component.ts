import { Component } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { ConfirmationService } from 'primeng/primeng';

import { DatabaseService } from '../../database.service';

@Component({
  templateUrl: './coupon-single.component.html',
  styleUrls: ['./coupon-single.component.scss'],
  providers: [ConfirmationService]
})
export class CouponSingleComponent {
  public displayLoading = false;
  public errorMessages = [];
  public couponId = this.activatedRoute.snapshot.params['id'];
  public coupon: any = {};

  constructor(
    private activatedRoute: ActivatedRoute,
    private databaseService: DatabaseService,
    private router: Router,
    private confirmationService: ConfirmationService
  ) {
    this.coupon = this.databaseService.getCouponItem(this.couponId).then(coupon => {
      this.coupon = coupon;
    });
  }

  deleteButtonClicked() {
    this.confirmationService.confirm({
      message: 'Do you want to delete this coupon?',
      header: 'Delete Confirmation',
      icon: 'fa fa-trash',
      accept: () => {
        this.processDelete();
      }
    });
  }

  processDelete() {
    this.errorMessages = [];
    this.displayLoading = true;

    this.databaseService.deleteCouponItem(this.couponId).then(() => {
      this.displayLoading = false;
      this.router.navigate(['/admin/coupon-all']);
    }).catch(err => {
      this.displayLoading = false;
      this.errorMessages.push({
        severity: 'error',
        summary: 'Something went wrong!',
        detail: err
      });
    });
  }
}
