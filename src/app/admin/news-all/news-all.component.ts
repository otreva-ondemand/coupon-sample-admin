import { Component } from '@angular/core';
import { DatabaseService } from '../../database.service';

@Component({
  templateUrl: './news-all.component.html',
  styleUrls: ['./news-all.component.scss']
})
export class NewsAllComponent {
  public newsItems;

  constructor(private databaseService: DatabaseService) {
    this.newsItems = this.databaseService.getNews();
  }
}
