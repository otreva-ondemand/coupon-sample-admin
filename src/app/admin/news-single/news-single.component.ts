import { Component } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { ConfirmationService } from 'primeng/primeng';

import { DatabaseService } from '../../database.service';

@Component({
  templateUrl: './news-single.component.html',
  styleUrls: ['./news-single.component.scss'],
  providers: [ConfirmationService]
})
export class NewsSingleComponent {
  public displayLoading = false;
  public errorMessages = [];
  public newsId = this.activatedRoute.snapshot.params['id'];
  public newsItem: any = {};

  constructor(
    private activatedRoute: ActivatedRoute,
    private databaseService: DatabaseService,
    private router: Router,
    private confirmationService: ConfirmationService
  ) {
    this.newsItem = this.databaseService.getNewsItem(this.newsId)
      .then(newsItem => {this.newsItem = newsItem});
  }

  deleteButtonClicked() {
    this.confirmationService.confirm({
      message: 'Are you sure you want to delete this News & Sale?',
      header: 'Delete News & Sale?',
      icon: 'fa fa-trash',
      accept: () => {this.processDelete()}
    })
  }

  processDelete() {
    this.errorMessages = [];
    this.displayLoading = true;

    this.databaseService.deleteNewsItem(this.newsId)
      .then(() => {
        this.displayLoading = false;
        this.router.navigate(['/admin/news-all']);
      })
      .catch(error => {
        this.displayLoading = false;
        this.errorMessages.push({
          severity: 'error',
          summary: 'Something went wrong!',
          detail: error
        })
      })
  }
}
