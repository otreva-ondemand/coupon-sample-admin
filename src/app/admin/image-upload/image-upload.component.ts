import { Component } from '@angular/core';
import { ChangeDetectorRef } from '@angular/core'

import { StorageService } from '../../storage.service';

@Component({
  selector: 'app-image-upload',
  templateUrl: './image-upload.component.html',
  styleUrls: ['./image-upload.component.scss']
})
export class ImageUploadComponent {
  public imageBase64 = '';

  constructor(
    private changeDetectorRef: ChangeDetectorRef,
    private storageService: StorageService
  ) {}

  deleteButtonClicked() {
    this.imageBase64 = '';
    this.storageService.clearImageBase64();
  }

  chooseFileClicked(input) {
    if (input.files[0] === undefined) return;
    this.readFile(input.files[0]);
  }

  readFile(file) {
    let reader = new FileReader();

    // Read the file
    reader.readAsDataURL(file);

    // fired once file is loaded
    reader.onload = () => {
      this.imageBase64 = reader.result;
      this.storageService.setImageBase64(this.imageBase64);
    }
  }

  ngOnDestroy() {
    this.storageService.clearImageBase64();
  }
}
