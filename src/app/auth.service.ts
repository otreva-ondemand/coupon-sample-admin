import { Injectable, Inject } from '@angular/core';
import { AngularFire, FirebaseApp } from 'angularfire2';
import { Router } from '@angular/router';

@Injectable()
export class AuthService {
  private user;
  private firebaseApp;

  constructor(
    @Inject(FirebaseApp) firebaseApp,
    private angularFire: AngularFire,
    private router: Router
  ) {
    this.firebaseApp = firebaseApp;
    this.handleAuchChanges();
  }

  handleAuchChanges() {
    this.angularFire.auth.subscribe(auth => {
      if (!auth) {
        this.router.navigate(['/login']);
      } else {
        this.user = auth;
      }
    });
  }

  isLoggedIn() {
    return new Promise((resolve, reject) => {
      let observable = this.angularFire.auth.subscribe(auth => {
        if (auth) {
          resolve();
          observable.unsubscribe();
        } else {
          reject();
          observable.unsubscribe();
        }
      });
    });
  }

  userLogin(email, password) {
    email = email.trim();

    return new Promise((resolve, reject) => {
      this.angularFire.auth.login({email: email, password: password}).then(() => {
        resolve();
        this.router.navigate(['/admin']);
      }).catch(err => {
        reject(err);
      });
    });
  }

  logout() {
    this.angularFire.auth.logout();
  }

  getUserEmail() {
    return this.user.auth.email;
  }

  changePassword(passwordObj) {
    return new Promise((resolve, reject) => {
      if (passwordObj.newPassword !== passwordObj.confirmNewPassword) {
        reject('New passwords do not match');
      }

      this.reauthenticateUser(passwordObj.currentPassword).then(() => {
        this.setUserPassword(passwordObj.newPassword).then(() => {
          resolve();
        }).catch(err => {
          reject(err);
        });
      }).catch(err => {
        reject(err);
      });
    });
  }

  reauthenticateUser(password) {
    return new Promise((resolve, reject) => {
      this.angularFire.auth.login({email: this.getUserEmail(), password: password}).then(() => {
        resolve();
      }).catch(err => {
        reject(err);
      });
    });
  }

  setUserPassword(password) {
    return new Promise((resolve, reject) => {
      this.firebaseApp.auth().currentUser.updatePassword(password).then(() => {
        resolve();
      }).catch(err => {
        reject(err);
      });
    });
  }

  triggerPasswordReset(email) {
    email = email.trim();

    return new Promise((resolve, reject) => {
      this.firebaseApp.auth().sendPasswordResetEmail(email).then(() => {
        resolve();
      }).catch(err => {
        reject(err);
      });
    });
  }
}
