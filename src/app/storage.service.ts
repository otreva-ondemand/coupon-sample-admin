import { Injectable, Inject } from '@angular/core';
import { FirebaseApp } from 'angularfire2';

@Injectable()
export class StorageService {
  public storageRef;
  public imageBase64 = '';

  constructor(@Inject(FirebaseApp) firebaseApp) {
    this.storageRef = firebaseApp.storage().ref();
  }

  setImageBase64(imageBase64) {
    this.imageBase64 = imageBase64.split(',')[1];
  }

  getImageBase64() {
    return this.imageBase64;
  }

  clearImageBase64() {
    this.imageBase64 = '';
  }

  uploadImage(imagePath) {
    return new Promise((resolve, reject) => {
      this.storageRef.child(imagePath).putString(this.imageBase64, 'base64').then(() => {
        resolve();
        this.clearImageBase64();
      }).catch(err => {
        reject(err);
      });
    });
  }

  hasImage() {
    if (this.imageBase64 === '') {
      return false;
    } else {
      return true;
    }
  }

  getImageUrl(imagePath) {
    return new Promise((resolve, reject) => {
      this.storageRef.child(imagePath).getDownloadURL().then(url => {
        resolve(url);
      }).catch(err => {
        reject(err);
      });
    });
  }

  deleteImage(imagePath) {
    return new Promise((resolve, reject) => {
      this.storageRef.child(imagePath).delete().then(() => {
        resolve();
      }).catch(err => {
        reject(err);
      });
    });
  }
}
