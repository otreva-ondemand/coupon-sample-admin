// dear future developers; sorry

import { Injectable } from '@angular/core';
import { AngularFire } from 'angularfire2';

import { AuthService } from './auth.service';
import { StorageService } from './storage.service';

@Injectable()
export class DatabaseService {
  private couponList;
  private newsList;

  constructor(
    private angularFire: AngularFire,
    private authService: AuthService,
    private storageService: StorageService
  ) {
    this.newsList = this.angularFire.database.list('/posts', {
      query: {
        orderByChild: 'createdDate'
      }
    });

    this.couponList = this.angularFire.database.list('/coupons', {
      query: {
        orderByChild: 'createdDate'
      }
    });
  }

  /*
     ######   #######  ##     ## ########   #######  ##    ##  ######
    ##    ## ##     ## ##     ## ##     ## ##     ## ###   ## ##    ##
    ##       ##     ## ##     ## ##     ## ##     ## ####  ## ##
    ##       ##     ## ##     ## ########  ##     ## ## ## ##  ######
    ##       ##     ## ##     ## ##        ##     ## ##  ####       ##
    ##    ## ##     ## ##     ## ##        ##     ## ##   ### ##    ##
     ######   #######   #######  ##         #######  ##    ##  ######
  */

  getCoupons() {
    return this.couponList;
  }

  getCouponItem(id) {
    let couponObservable = this.angularFire.database.list('/coupons', {
      query: {
        orderByKey: true,
        equalTo: id
      }
    });

    return new Promise((resolve, reject) => {
      let observable = couponObservable.subscribe(obj => {
        obj.map(coupon => {
          observable.unsubscribe();
          let couponData: any = {
            code: coupon.code,
            title: coupon.title,
            description: coupon.description,
            startDate: coupon.startDate,
            expDate: coupon.expDate,
            createdDate: coupon.createdDate,
            createdBy: coupon.createdBy,
            image: coupon.image,
            imagePath: coupon.imagePath
          }

          if (coupon.editedBy) couponData.editedBy = coupon.editedBy;
          if (coupon.editedDate) couponData.editedDate = coupon.editedDate;

          if (coupon.startDate === '') {
            couponData.startDate = '';
          } else {
            couponData.startDate = new Date(coupon.startDate);
          }

          if (coupon.expDate === '') {
            couponData.expDate = '';
          } else {
            couponData.expDate = new Date(coupon.expDate);
          }

          resolve(couponData);
        })
      })
    })
  }

  pushCoupon(coupon) {
    return new Promise((resolve, reject) => {
      coupon.code = coupon.code.toUpperCase();

      if (coupon.code === '' || coupon.title === '') {
        reject('You must supply a title and a coupon code');
        return;
      }

      if (/[.#$\[\] ]/g.test(coupon.code)) {
        reject('Coupon code cannot contain ".", "#", "$", "[", "]", or a space');
        return;
      }

      let couponsWithSameCode = this.angularFire.database.list('/coupons', {
        query: {
          orderByChild: 'code',
          equalTo: coupon.code
        },
        preserveSnapshot: true
      });

      let couponObservable = couponsWithSameCode.subscribe(snapshot => {
        couponObservable.unsubscribe();
        if (snapshot.length > 0) {
          reject('Sorry, but this code is already taken. Please choose another');
          return;
        } else {
          if (this.storageService.hasImage()) {
            this.pushCouponImageToStorage(coupon).then(() => {
              resolve();
            }).catch(err => {
              reject(err);
            });
          } else {
            this.pushCouponItemToDb(coupon).then(() => {
              resolve()
            }).catch(err => {
              reject(err);
            });
          }
        }
      });
    });
  }

  pushCouponItemToDb(coupon) {
    return new Promise((resolve, reject) => {
      this.couponList.push({
        code: coupon.code,
        title: coupon.title,
        description: coupon.description,
        image: coupon.image,
        imagePath: coupon.imagePath,
        startDate: coupon.startDate.toString(),
        expDate: coupon.expDate.toString(),
        createdDate: Date.now(),
        createdBy: this.authService.getUserEmail()
      });

      resolve();
    })
  }

  pushCouponImageToStorage(coupon) {
    return new Promise((resolve, reject) => {
      let imagePath = this.getCouponImagePath();

      this.storageService.uploadImage(imagePath)
        .then(() => {
          this.storageService.getImageUrl(imagePath)
            .then(imageUrl => {
              coupon.image = imageUrl;
              coupon.imagePath = imagePath;
              this.pushCouponItemToDb(coupon)
                .then(() => {resolve()})
            })
            .catch((error => {reject(error)}))
        })
        .catch(error => {reject(error)})
    })
  }

  getCouponImagePath() {
    return 'coupons/' + new Date().getTime();
  }

  deleteCouponItem(id) {
    return new Promise((resolve, reject) => {
      this.getCouponItem(id).then(item => {
        if (item['imagePath'] !== '') {
          this.storageService.deleteImage(item['imagePath']).then(() => {
            this.couponList.remove(id);
            resolve();
          }).catch(err => {
            reject(err);
          });
        } else {
          this.couponList.remove(id);
          resolve();
        }
      }).catch(err => {
        reject(err);
      });
    });
  }

  editCouponItem(couponId, couponObj, deleteImage, originalCouponCode) {
    couponObj.code = couponObj.code.toUpperCase();

    return new Promise((resolve, reject) => {
      if (couponObj.title === '' || couponObj.code === '') {
        reject('You must supply a title and a coupon code');
        return;
      }

      if (/[.#$\[\] ]/g.test(couponObj.code)) {
        reject('Coupon code cannot contain ".", "#", "$", "[", "]", or a space');
        return;
      }

      let couponsWithSameCode = this.angularFire.database.list('/coupons', {
        query: {
          orderByChild: 'code',
          equalTo: couponObj.code
        },
        preserveSnapshot: true
      });

      let couponObservable = couponsWithSameCode.subscribe(snapshot => {
        couponObservable.unsubscribe();

        if (couponObj.code !== originalCouponCode) {
          if (snapshot.length > 0) {
            reject('Sorry, but this code is already taken. Please choose another');
            return;
          }
        }

        // actual logic
        if (deleteImage) {
          this.storageService.deleteImage(couponObj.imagePath);
          couponObj.image = '';
          couponObj.imagePath = '';
        }

        if (this.storageService.hasImage()) {
          this.pushEditedCouponImageToStorage(couponId, couponObj).then(() => {
            resolve();
          }).catch(err => {
            reject(err);
          });
        } else {
          this.pushEditedCouponToDb(couponId, couponObj).then(() => {
            resolve();
          }).catch(err => {
            reject(err);
          });
        }

      });
    })
  }

  pushEditedCouponToDb(code, couponObj) {
    return new Promise((resolve, reject) => {
      couponObj.editedDate = new Date().getTime();
      couponObj.editedBy = this.authService.getUserEmail();
      this.angularFire.database.object('/coupons/' + code).update(couponObj);
      resolve();
    })
  }

  pushEditedCouponImageToStorage(code, couponObj) {
    return new Promise((resolve, reject) => {
      let imagePath = this.getCouponImagePath();

      this.storageService.uploadImage(imagePath)
        .then(() => {
          this.storageService.getImageUrl(imagePath)
            .then(imageUrl => {
              couponObj.image = imageUrl;
              couponObj.imagePath = imagePath;
              this.pushEditedCouponToDb(code, couponObj)
                .then(() => {resolve()})
            })
            .catch((error => {reject(error)}))
        })
        .catch(error => {reject(error)})
    })
  }

  /*
    ##    ## ######## ##      ##  ######
    ###   ## ##       ##  ##  ## ##    ##
    ####  ## ##       ##  ##  ## ##
    ## ## ## ######   ##  ##  ##  ######
    ##  #### ##       ##  ##  ##       ##
    ##   ### ##       ##  ##  ## ##    ##
    ##    ## ########  ###  ###   ######
  */

  getNews() {
    return this.newsList;
  }

  getNewsItem(id) {
    let newsItemObservable = this.angularFire.database.list('/posts', {
      query: {
        orderByKey: true,
        equalTo: id
      }
    })

    return new Promise((resolve, reject) => {
      let observable = newsItemObservable.subscribe(obj => {
        obj.map(post => {
          observable.unsubscribe();
          let newsItemData: any = {
            title: post.title,
            content: post.content,
            createdBy: post.createdBy,
            createdDate: post.createdDate,
            image: post.image,
            imagePath: post.imagePath
          }
          if (post.editedBy) newsItemData.editedBy = post.editedBy;
          if (post.editedDate) newsItemData.editedDate = post.editedDate;
          resolve(newsItemData);
        })
      })
    })
  }

  editNewsItem(id, newsObj, deleteImage) {
    return new Promise((resolve, reject) => {
      if (newsObj.title === '') {
        reject('News posts cannot have a blank title, I hope you understand..');
        return;
      }

      if (deleteImage) {
        this.storageService.deleteImage(newsObj.imagePath);
        newsObj.image = '';
        newsObj.imagePath = '';
      }

      if (this.storageService.hasImage()) {
        this.pushEditedNewsImageToStorage(id, newsObj)
          .then(() => {resolve()})
          .catch((error) => {reject(error)});
      } else {
        this.pushEditedNewsToDb(id, newsObj)
          .then(() => {resolve()})
          .catch((error) => {reject(error)});
      }
    })
  }

  pushEditedNewsImageToStorage(id, newsItem) {
    return new Promise((resolve, reject) => {
      let imagePath = this.getNewsImagePath();

      this.storageService.uploadImage(imagePath)
        .then(() => {
          this.storageService.getImageUrl(imagePath)
            .then(imageUrl => {
              newsItem.image = imageUrl;
              newsItem.imagePath = imagePath;
              this.pushEditedNewsToDb(id, newsItem)
                .then(() => {resolve()})
            })
            .catch((error => {reject(error)}))
        })
        .catch(error => {reject(error)})
    })
  }

  pushEditedNewsToDb(id, newsObj) {
    return new Promise((resolve, reject) => {
      newsObj.editedDate = new Date().getTime();
      newsObj.editedBy = this.authService.getUserEmail();
      this.angularFire.database.object('/posts/' + id).update(newsObj);
      resolve();
    })
  }

  deleteNewsItem(id) {
    return new Promise((resolve, reject) => {
      this.getNewsItem(id)
        .then(item => {
          if (item['imagePath'] !== '') {
            this.storageService.deleteImage(item['imagePath'])
              .then(() => {
                this.newsList.remove(id);
                resolve();
              })
          } else {
            this.newsList.remove(id);
            resolve();
          }
        })
    })
  }

  pushNewsItem(newsItem) {
    return new Promise((resolve, reject) => {
      if (newsItem.title === '') {
        reject('We\'re terribly sorry, but we cannot proceed without a title.');
        return;
      }

      if (this.storageService.hasImage()) {
        this.pushNewsImageToStorage(newsItem)
          .then(() => {resolve()})
      } else {
        this.pushNewsItemToDb(newsItem)
          .then(() => {resolve()})
      }
    })
  }

  getNewsImagePath() {
    return 'posts/' + new Date().getTime();
  }

  pushNewsItemToDb(newsItem) {
    return new Promise((resolve, reject) => {
      this.newsList.push({
        title: newsItem.title,
        content: newsItem.content,
        image: newsItem.image,
        imagePath: newsItem.imagePath,
        createdDate: Date.now(),
        createdBy: this.authService.getUserEmail()
      })

      resolve();
    })
  }

  pushNewsImageToStorage(newsItem) {
    return new Promise((resolve, reject) => {
      let imagePath = this.getNewsImagePath();

      this.storageService.uploadImage(imagePath)
        .then(() => {
          this.storageService.getImageUrl(imagePath)
            .then(imageUrl => {
              newsItem.image = imageUrl;
              newsItem.imagePath = imagePath;
              this.pushNewsItemToDb(newsItem)
                .then(() => {resolve()})
            })
            .catch((error => {reject(error)}))
        })
        .catch(error => {reject(error)})
    })
  }

  /*
     ######   #######   ######  ####    ###    ##
    ##    ## ##     ## ##    ##  ##    ## ##   ##
    ##       ##     ## ##        ##   ##   ##  ##
     ######  ##     ## ##        ##  ##     ## ##
          ## ##     ## ##        ##  ######### ##
    ##    ## ##     ## ##    ##  ##  ##     ## ##
     ######   #######   ######  #### ##     ## ########
  */

  pushSocial(urls) {
    // TODO: consider trimming these urls becuase users suck
    return new Promise((resolve, reject) => {
      this.angularFire.database.object('/social').set(urls).then(() => {
        resolve();
      }).catch(err => {
        reject(err);
      });
    });
  }

  getSocial() {
    let urlsRef = this.angularFire.database.object('/social', {preserveSnapshot: true});

    return new Promise((resolve, reject) => {
      let subscriber = urlsRef.subscribe(snapshot => {
        subscriber.unsubscribe();
        resolve({
          facebook: snapshot.val().facebook,
          twitter: snapshot.val().twitter,
          googlePlus: snapshot.val().googlePlus,
          youTube: snapshot.val().youTube,
          instagram: snapshot.val().instagram,
          pinterest: snapshot.val().pinterest,
          tumblr: snapshot.val().tumblr,
          website: snapshot.val().website
        });
      });
    });
  }
}
