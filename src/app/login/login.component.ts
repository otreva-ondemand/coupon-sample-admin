import { Component } from '@angular/core';

import { AuthService } from '../auth.service';

@Component({
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent {
  public isResetDisabled = false;
  public messages = [];
  public isUserResettingPassword = false;
  public model = {
    email: '',
    password: ''
  }

  constructor(private authService: AuthService) {}

  forgotPasswordClicked() {
    this.isUserResettingPassword = true;
    this.messages = [];
  }

  cancelForgotPasswordClicked() {
    this.isUserResettingPassword = false;
    this.messages = [];
  }

  resetButtonClicked() {
    this.messages = [];
    this.isResetDisabled = true;

    this.authService.triggerPasswordReset(this.model.email)
      .then(() => {
        this.isResetDisabled = false;
        this.isUserResettingPassword = false;
        this.messages.push({
          severity: 'success',
          summary: 'WooHoo!',
          detail: 'Your password reset email is on its way!'
        })
      })
      .catch(error => {
        this.isResetDisabled = false;
        this.messages.push({
          severity: 'error',
          summary: 'Something went wrong!',
          detail: error
        })
      })
  }

  loginButtonClicked() {
    this.messages = [];
    this.authService.userLogin(this.model.email, this.model.password)
      .catch(error => {
        this.messages.push({
          severity: 'error',
          summary: 'Something went wrong!',
          detail: error
        })
      })
  }
}
