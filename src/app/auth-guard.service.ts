import { Injectable } from '@angular/core';
import { RouterStateSnapshot, ActivatedRouteSnapshot } from '@angular/router';

import { AuthService } from './auth.service';

@Injectable()
export class AuthGuard {
  constructor(private authService: AuthService) {}

  canActivate(
    activatedRouteSnapshot: ActivatedRouteSnapshot,
    routerStateSnapshot: RouterStateSnapshot
  ) {
    return this.checkLogin();
  }

  checkLogin() {
    return new Promise((resolve, reject) => {
      this.authService.isLoggedIn()
        .then(() => {
          resolve(true);
        })
        .catch(() => {
          resolve(false);
        })
    })
  }
}
