import { CouponSampleAdminPage } from './app.po';

describe('coupon-sample-admin App', function() {
  let page: CouponSampleAdminPage;

  beforeEach(() => {
    page = new CouponSampleAdminPage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
